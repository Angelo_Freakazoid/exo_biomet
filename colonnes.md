# Nom des colonnes 

#  1 Biacromial diameter = Bia_diam
#  2 Biiliac diameter, or "pelvic breadth" = Bii_diam
#  3 Bitrochanteric diameter = Bit_diam
#  4 Chest depth between spine and sternum at nipple level, mid-expiration = Chest_depth
#  5 Chest diameter at nipple level, mid-expiration = Chest
#  6 Elbow diameter, sum of two elbows = Elbow
#  7 Wrist diameter, sum of two wrists = Wrist
#  8 Knee diameter, sum of two knees = Knee
#  9 Ankle diameter, sum of two ankles = Ankle
# 10 Shoulder girth over deltoid muscles = Shoulder
# 11 Chest girth, nipple line in males and just above breast tissue in females, mid-expiration = Chest_girth
# 12 Waist girth, narrowest part of torso below the rib cage, average of contracted and relaxed position = Waist
# 13 Navel (or "Abdominal") girth at umbilicus and iliac crest, iliac crest as a landmark = Navel
# 14 Hip girth at level of bitrochanteric diameter = Hip
# 15 Thigh girth below gluteal fold, average of right and left girths = Thigh_girth
# 16 Bicep girth, flexed, average of right and left girths = Bicep
# 17 Forearm girth, extended, palm up, average of right and left girths = Forearm
# 18 Knee girth over patella, slightly flexed position, average of right and left girths = Knee_girth
# 19 Calf maximum girth, average of right and left girths = Calf_max_girth
# 20 Ankle minimum girth, average of right and left girths = Ankle_min_girth
# 21 Wrist minimum girth, average of right and left girths = Wrist_min_girth
# 22 Age (years) = Age
# 23 Weight (kg) = Weight
# 24 Height (cm) = Height
# 25 Gender (1 - male, 0 - female) = Gender_M_F