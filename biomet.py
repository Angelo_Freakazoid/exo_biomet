import pandas as pd

biomet_list = '/home/user/bioart/exo_biomet/biomet_list'

with open(biomet_list, 'r') as txt:
    colonnes = txt.readlines()

colonnes_split = [colonne.split() for colonne in colonnes]

nom_colonne = [
    "Bia_diam",
    "Bii_diam",
    "Bit_diam",
    "Chest_depth",
    "Chest",
    "Elbow",
    "Wrist",
    "Knee",
    "Ankle",
    "Shoulder",
    "Chest_girth",
    "Waist",
    "Navel",
    "Hip",
    "Thigh_girth",
    "Bicep",
    "Forearm",
    "Knee_girth",
    "Calf_max_girth",
    "Ankle_min_girth",
    "Wrist_min_girth",
    "Age",
    "Weight",
    "Height",
    "Gender_M_F"
]

df = pd.DataFrame(colonnes_split, columns=nom_colonne)
df = df.astype(float)

